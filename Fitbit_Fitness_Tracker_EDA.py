#!/usr/bin/env python
# coding: utf-8

# # Fitbit Fitness Tracker EDA
# #By- Aarush Kumar
# #Dated: July 20,2021

# In[2]:


get_ipython().system('pip install ipython')


# In[5]:


from IPython.display import Image
Image(url='https://cdn.wallpapersafari.com/45/73/7tDm2K.png')


# In[6]:


import pandas as pd
import numpy as np
import seaborn as sns


# In[7]:


activity = pd.read_csv('/home/aarush100616/Downloads/Projects/Fitbit EDA/FitBit data.csv')


# In[8]:


activity


# In[10]:


get_ipython().system('pip install pandas-profiling')


# In[11]:


import pandas_profiling


# In[12]:


activity.profile_report() 


# In[13]:


activity.shape


# In[14]:


activity.isnull().sum()


# In[15]:


activity1 = activity.copy()


# In[16]:


activity1['ActivityDate'].unique() 


# In[17]:


activity1['ActivityDate'].head(10) 


# In[18]:


# adding the yearm month and date columns to the dataset
activity1['year'] = pd.DatetimeIndex(activity1['ActivityDate']).year
activity1['month'] = pd.DatetimeIndex(activity1['ActivityDate']).month
activity1['date'] = pd.DatetimeIndex(activity1['ActivityDate']).day


# In[19]:


activity1.head(10)


# In[20]:


activity1=activity1.drop(['TrackerDistance'],axis=1)  #dropping the TrackerDistance column


# In[21]:


activity1.head(20)


# In[22]:


import datetime as dt


# In[23]:


import matplotlib.pyplot as plt
# figure size
plt.figure(figsize=(15,8))
# Usual boxplot
ax = sns.boxplot(x='date', y='Calories', data=activity1)
# Add jitter with the swarmplot function.
ax = sns.swarmplot(x='date', y='Calories', data=activity1, color="grey")
ax.set_title('Box plot of Calories with Jitter bu day of the month')


# In[24]:


# converting the datatype to datetime
activity1['Week'] = pd.to_datetime(activity1.ActivityDate).dt.week
activity1['Year'] = pd.to_datetime(activity1.ActivityDate).dt.year


# In[25]:


activity1.head()


# In[26]:


activity1.ActivityDate.dtype


# In[27]:


activity1['ActivityDate'] = pd.to_datetime(activity1['ActivityDate']) # converting it to datetime


# In[28]:


activity1['day'] = activity1['ActivityDate'].dt.day_name
# converting the day of the week to the name of the day


# In[29]:


activity1.head(10)


# In[30]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot
ax = sns.scatterplot(x='Calories', y='SedentaryMinutes', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[31]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot
ax = sns.scatterplot(x='Calories', y='LightlyActiveMinutes', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[32]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot between calories burnt in the moderately active minutes
ax = sns.scatterplot(x='Calories', y='FairlyActiveMinutes', data=activity1)
ax.set_title('Scatterplot of calories vs Fairly Active Minutes')


# In[33]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot between calories burnt in the intensely active minutes
ax = sns.scatterplot(x='Calories', y='VeryActiveMinutes', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[34]:


activity1=activity1.drop(['Week','Year'],axis=1) # dropping the columns week and year


# In[35]:


activity1.shape 


# In[36]:


## plot the raw values 
col_select = ['Calories','VeryActiveMinutes','FairlyActiveMinutes','LightlyActiveMinutes','SedentaryMinutes']
wide_df = activity1[col_select]
# figure size
plt.figure(figsize=(15,8))
# timeseries plot using lineplot
ax = sns.lineplot(data=wide_df)
ax.set_title('Un-normalized value of calories and different activities based on activity minutes')


# In[37]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot between  calories burnt and total distance covered
ax = sns.scatterplot(x='Calories', y='TotalDistance', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[38]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot between calories burnt and the loggged activities distance
ax = sns.scatterplot(x='Calories', y='LoggedActivitiesDistance', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[39]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot between calories burnt and the distance of intense activies
ax = sns.scatterplot(x='Calories', y='VeryActiveDistance', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[40]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot between calories burnt and the distance of moderate activies
ax = sns.scatterplot(x='Calories', y='ModeratelyActiveDistance', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[41]:


# figure size
plt.figure(figsize=(15,8))
# Simple scatterplot
ax = sns.scatterplot(x='Calories', y='LightActiveDistance', data=activity1)
ax.set_title('Scatterplot of calories and intense_activities')


# In[42]:


## plot the raw values 
rol_select = ['TotalDistance','LoggedActivitiesDistance','VeryActiveDistance','ModeratelyActiveDistance', 'LightActiveDistance']
wide_df1 = activity1[rol_select]
# figure size
plt.figure(figsize=(15,8))
# timeseries plot using lineplot
ax = sns.lineplot(data=wide_df1)
ax.set_title('Un-normalized value of calories and different activities based on distance')


# The EDA here gives us the insight about the relation between the active hours, the distance for which the user has moderate and intense activity and the calories burnt during that period.
